# RPG Character Game

## Configuration:
This application is build with Java v.17 on "Maven" model version 4.0.0 and uses JUnit v.4.13.2 and JUnit-Jupiter v.5.8.2.\
You can just clone repository and run the application locally. You must use an IDE like IntelliJ Idea. I personally usе IntelliJ Idea Ultimate.
The location of the Main class, which is the entry Point of the application, is: src\main\java\Main.java

The application has also Unit tests. Those can be run locally to. You can find them in: src\test\java\...

The MainController is not tested because of the amount of the terminal prompts. It will take a lot of time to test every output and command.
<br>
<br>

## Project architecture:
![main](https://user-images.githubusercontent.com/79804094/154220047-e7959c9d-01d0-4b23-9847-56e8bf471b76.png)

### Usage
The usage of the application is very simple. You can just run the main method, and then you must simply fallow the prompts,
coming from the terminal.

If you want to play, you can choose from 4 types of Characters.
The available character classes are:

- Mage
- Ranger
- Rogue
- Warrior

Once you've chosen your character class, you need to give it a name. Then, if you want, you have the option to equip you Hero.\
The fallowing items are available:

- <b>Weapons:</b>
  - Axe
  - Hammer
  - Sword
  - Dagger
  - Bow
  - Staff
  - Wand
  
- <b>Armor:</b>
  - <b>BodyArmor:</b>
    - Cloth
    - Leather
    - Mail
    - Plate
  - <b>Head Armor:</b>
    - Cap
    - Helm
  - <b>Hands Armor:</b>
    - Cloth Gloves
    - Leather Gloves
    - Mail Gloves
  - <b>Legs Armor:</b>
    - Boots
    - Mail Boots

<b>Beware of typing the items exactly as you see them below. The command is case-insensitive.

Example:\
LeatherGloves -> valid\
Leather Gloves -> not valid\
leathergloves -> valid\
leather gloves -> not valid</b>



When your hero is equipped, you are ready to start the game. Hire, the following commands are available:

<b>command: end  -> closes the application\
command: stats  -> show your character stats\
command: equipment -> show character's equipment\
command: explore  -> the main part of the game starts. You must go through a labyrinth</b>

In the main part of the game you must go through a labyrinth. This is how it looks like:

![lab](https://user-images.githubusercontent.com/79804094/154256872-6b875cfa-51ea-4624-a19b-8a569c8ef8cd.png)

<b>"p"</b> stays for current player position\
<b>"m"</b> stays for monster\
<b>"*"</b> stays for wall\
<b>"_"</b> stays for free path\
<b>"e"</b> stays exit from labyrinth

Your task is go through the labyrinth and reach the exit using only the commands described above

<b>"up"</b> -> your hero goes up\
<b>"down"</b> -> your hero goes down\
<b>"left"</b> -> your hero goes left\
<b>"right"</b> -> your hero goes right

When you came across a monster, you will fight and increase your level!\
When you came across an "e" sign, the game will stop and final Hero stats will be shown.


## Maintainer
[Petar Dimitrov]

## License
[MIT]

---
[Petar Dimitrov]: https://github.com/PetarDimitrov91
[MIT]: https://choosealicense.com/licenses/mit/



