
import common.AttrNames;
import common.Slots;
import exceptions.IllegalArmorException;
import exceptions.IllegalItemException;
import exceptions.IllegalWeaponException;
import models.characters.Mage;
import models.characters.Ranger;
import models.characters.Rogue;
import models.characters.Warrior;
import models.characters.interfaces.Hero;
import models.items.Item;
import models.items.armor.Armor;
import models.items.armor.body.*;
import models.items.armor.hands.ClothGloves;
import models.items.armor.hands.HandsArmor;
import models.items.armor.hands.LeatherGloves;
import models.items.armor.hands.MailGloves;
import models.items.armor.head.Cap;
import models.items.armor.head.HeadArmor;
import models.items.armor.head.Helm;
import models.items.armor.legs.Boots;
import models.items.armor.legs.LegsArmor;
import models.items.armor.legs.MailBoots;
import models.items.weapons.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static common.AttributeValues.*;
import static org.junit.Assert.*;

public class FunctionalityTest {
    private Hero mage;
    private Hero ranger;
    private Hero rogue;
    private Hero warrior;

    private Weapon axe;
    private Weapon bow;
    private Weapon dagger;
    private Weapon hammer;
    private Weapon staff;
    private Weapon sword;
    private Weapon wand;

    private BodyArmor cloth;
    private BodyArmor leather;
    private BodyArmor mail;
    private BodyArmor plate;

    private HandsArmor clothGloves;
    private HandsArmor leatherGloves;
    private HandsArmor mailGloves;

    private HeadArmor cap;
    private HeadArmor helm;

    private LegsArmor boots;
    private LegsArmor mailBoots;


    private static final String MAGE_NAME = "Petar";
    private static final String RANGER_NAME = "John";
    private static final String ROGUE_NAME = "Viktor";
    private static final String WARRIOR_NAME = "Sam";

    @Before
    public void setUp() {
        this.mage = new Mage(MAGE_NAME);
        this.ranger = new Ranger(RANGER_NAME);
        this.rogue = new Rogue(ROGUE_NAME);
        this.warrior = new Warrior(WARRIOR_NAME);

        this.axe = new Axe();
        this.bow = new Bow();
        this.dagger = new Dagger();
        this.hammer = new Hammer();
        this.staff = new Staff();
        this.sword = new Sword();
        this.wand = new Wand();

        this.cloth = new Cloth();
        this.leather = new Leather();
        this.mail = new Mail();
        this.plate = new Plate();

        this.clothGloves = new ClothGloves();
        this.leatherGloves = new LeatherGloves();
        this.mailGloves = new MailGloves();

        this.cap = new Cap();
        this.helm = new Helm();

        this.boots = new Boots();
        this.mailBoots = new MailBoots();
    }

    @Test
    public void constructorTests() {
        assertTrue(this.mage instanceof Mage);
        assertTrue(this.ranger instanceof Ranger);
        assertTrue(this.rogue instanceof Rogue);
        assertTrue(this.warrior instanceof Warrior);
    }

    @Test
    public void mageGetName_returnsCorrectValue() {
        String actual = this.mage.getName();
        assertEquals(MAGE_NAME, actual);
    }

    @Test
    public void rangerGetName_returnsCorrectValue() {
        String actual = this.ranger.getName();
        assertEquals(RANGER_NAME, actual);
    }

    @Test
    public void rogueGetName_returnsCorrectValue() {
        String actual = this.rogue.getName();
        assertEquals(ROGUE_NAME, actual);
    }

    @Test
    public void warriorGetName_returnsCorrectValue() {
        String actual = this.warrior.getName();
        assertEquals(WARRIOR_NAME, actual);
    }

    @Test
    public void getPrimeAttr_returnsCorrectValue() {
        int actual = this.ranger.getPrimAttr().getValue();
        assertEquals(RANGER_DEXTERITY_PRM, actual);

        this.ranger.levelUp();

        actual = this.ranger.getPrimAttr().getValue();
        assertEquals(RANGER_DEXTERITY_PRM + RANGER_DEXTERITY_PRM_LVL_UP, actual);
    }

    @Test
    public void getTotalPrimeAttr_returnsCorrectValue() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        int actual = this.ranger.getTotalPrimAttr().getValue();
        assertEquals(RANGER_DEXTERITY_PRM, actual);
        this.ranger.levelUp();
        ranger.setBodyArmor(this.mail);

        actual = this.ranger.getTotalPrimAttr().getValue();
        assertEquals(PRIME_MAIL_ATTR_VAL + RANGER_DEXTERITY_PRM + RANGER_DEXTERITY_PRM_LVL_UP, actual);
    }

    @Test
    public void mageLevelFunctionality_returnsCorrectValue() {
        int actual = this.mage.getLevel();
        assertEquals(1, actual);

        this.mage.levelUp();

        actual = this.mage.getLevel();
        assertEquals(2, actual);
    }

    @Test
    public void rangerLevelFunctionality_returnsCorrectValue() {
        int actual = this.ranger.getLevel();
        assertEquals(1, actual);

        this.ranger.levelUp();

        actual = this.ranger.getLevel();
        assertEquals(2, actual);
    }

    @Test
    public void rogueLevelFunctionality_returnsCorrectValue() {
        int actual = this.rogue.getLevel();
        assertEquals(1, actual);

        this.rogue.levelUp();

        actual = this.rogue.getLevel();
        assertEquals(2, actual);
    }

    @Test
    public void warriorLevelFunctionality_returnsCorrectValue() {
        int level = this.warrior.getLevel();
        assertEquals(1, level);

        this.warrior.levelUp();

        level = this.warrior.getLevel();
        assertEquals(2, level);
    }

    @Test
    public void setHeadArmor_setsItemInCorrectSlot() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        Item actualNull = this.mage.getHeadArmor();
        Assert.assertNull(actualNull);

        this.mage.setHeadArmor(this.cap);

        Item actualArmor = this.mage.getHeadArmor();
        Assert.assertNotNull(actualArmor);

        Item actualArmorNotNull = this.mage.getHeadArmor();
        assertEquals(this.cap, actualArmorNotNull);

        Slots actualSlot = this.mage.getHeadArmor().getSlot();

        assertEquals(Slots.HEAD, actualSlot);
    }

    @Test
    public void setBodyArmor_setsItemInCorrectSlot() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        Item actualNull = this.mage.getBodyArmor();
        Assert.assertNull(actualNull);

        this.mage.setBodyArmor(this.cloth);

        Item actualArmorNotNull = this.mage.getBodyArmor();
        Assert.assertNotNull(actualArmorNotNull);

        Item actualBodyArmor = this.mage.getBodyArmor();
        assertEquals(this.cloth, actualBodyArmor);

        Slots actualSlot = this.mage.getBodyArmor().getSlot();
        assertEquals(Slots.BODY, actualSlot);
    }

    @Test
    public void setHandsArmor_setsItemInCorrectSlot() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        Item actualNull = this.mage.getHandsArmor();
        Assert.assertNull(actualNull);

        this.mage.setHandsArmor(this.clothGloves);

        Item actualNotNull = this.mage.getHandsArmor();
        Assert.assertNotNull(actualNotNull);

        Item actualArmor = this.mage.getHandsArmor();
        assertEquals(this.clothGloves, actualArmor);

        Slots actualSlot = this.mage.getHandsArmor().getSlot();
        assertEquals(Slots.HANDS, actualSlot);
    }

    @Test
    public void setLegsArmor_setsItemInCorrectSlot() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        Item actualNull = this.mage.getLegsArmor();
        Assert.assertNull(actualNull);

        this.mage.setLegsArmor(this.boots);

        Item actualNotNull = this.mage.getLegsArmor();
        Assert.assertNotNull(actualNotNull);

        Item actualArmor = this.mage.getLegsArmor();
        assertEquals(this.boots, actualArmor);

        Slots actualSlot = this.mage.getLegsArmor().getSlot();
        assertEquals(Slots.LEGS, actualSlot);
    }

    @Test
    public void setWeapon_setsItemInCorrectSlot() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        Weapon actualNull = this.mage.getWeapon();
        Assert.assertNull(actualNull);

        this.mage.setWeapon(this.staff);

        Weapon actualNotNull = this.mage.getWeapon();
        Assert.assertNotNull(actualNotNull);

        Weapon actualWeapon = this.mage.getWeapon();
        assertEquals(this.staff, actualWeapon);

        Slots actualSlot = this.mage.getWeapon().getSlot();
        assertEquals(Slots.WEAPON, actualSlot);
    }


    @Test(expected = IllegalArmorException.class)
    public void setHeadArmor_setNotCorrectItemThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.setHeadArmor(this.helm);
    }

    @Test(expected = IllegalArmorException.class)
    public void setBodyArmor_setNotCorrectItemThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.ranger.setBodyArmor(this.plate);
    }

    @Test(expected = IllegalArmorException.class)
    public void setHandsArmor_setNotCorrectItemThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.rogue.setHeadArmor(this.mailGloves);
    }

    @Test(expected = IllegalArmorException.class)
    public void setLegsArmor_setNotCorrectItemThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.setHeadArmor(this.mailBoots);
    }

    @Test(expected = IllegalWeaponException.class)
    public void setWeapon_setNotCorrectItemThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.warrior.setWeapon(this.staff);
    }

    @Test(expected = IllegalItemException.class)
    public void setWeapon_setItemWithLevelGreaterFormAllowedThrows() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.warrior.setWeapon(this.hammer);
    }

    @Test
    public void testsIfExistingItemWillBeStoredWhenAnItemIsSet() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.warrior.setWeapon(this.sword);
        this.warrior.levelUp();
        this.warrior.setWeapon(this.axe);

        List<Item> items = this.warrior.getInventory().getItems();

        assertTrue(items.size() > 0);
        assertEquals(this.sword, items.get(0));
    }

    @Test
    public void mageGetBaseAttrs_returnsCorrect() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, MAGE_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, MAGE_DEXTERITY);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, MAGE_INTELLIGENCE_PRM);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.mage.getBaseAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void rangerGetBaseAttrs_returnsCorrect() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, RANGER_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, RANGER_DEXTERITY_PRM);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, RANGER_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.ranger.getBaseAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void rogueGetBaseAttrs_returnsCorrect() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, ROGUE_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, ROGUE_DEXTERITY_PRM);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, ROGUE_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.rogue.getBaseAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void warriorWarriorGetBaseAttrs_returnsCorrect() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, WARRIOR_STRENGTH_PRM);
        mock_mageAttrs.put(AttrNames.DEXTERITY, WARRIOR_DEXTERITY);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, WARRIOR_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.warrior.getBaseAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void mageGetTotalAttrs_returnsCorrect_withoutEquipment() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, MAGE_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, MAGE_DEXTERITY);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, MAGE_INTELLIGENCE_PRM);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.mage.getTotalAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void rangerGetTotalAttrs_returnsCorrect_withoutEquipment() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, RANGER_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, RANGER_DEXTERITY_PRM);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, RANGER_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.ranger.getTotalAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void rogueGetTotalAttrs_returnsCorrect_withoutEquipment() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, ROGUE_STRENGTH);
        mock_mageAttrs.put(AttrNames.DEXTERITY, ROGUE_DEXTERITY_PRM);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, ROGUE_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.rogue.getTotalAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void warriorWarriorGetTotalAttrs_returnsCorrect_withoutEquipment() {
        Map<AttrNames, Integer> mock_mageAttrs = new HashMap<>();

        mock_mageAttrs.put(AttrNames.STRENGTH, WARRIOR_STRENGTH_PRM);
        mock_mageAttrs.put(AttrNames.DEXTERITY, WARRIOR_DEXTERITY);
        mock_mageAttrs.put(AttrNames.INTELLIGENCE, WARRIOR_INTELLIGENCE);
        mock_mageAttrs.put(AttrNames.VITALITY, DEF_HERO_VITALITY);

        Map<AttrNames, Integer> allAttrs = this.warrior.getTotalAttrs().getAllAttrs();

        assertEquals(mock_mageAttrs.get(AttrNames.STRENGTH), allAttrs.get(AttrNames.STRENGTH));
        assertEquals(mock_mageAttrs.get(AttrNames.DEXTERITY), allAttrs.get(AttrNames.DEXTERITY));
        assertEquals(mock_mageAttrs.get(AttrNames.INTELLIGENCE), allAttrs.get(AttrNames.INTELLIGENCE));
        assertEquals(mock_mageAttrs.get(AttrNames.VITALITY), allAttrs.get(AttrNames.VITALITY));
    }

    @Test
    public void getDPS_withoutEquipment_returnsCorrectValue() {
        double expectedValue = 1 * (1 + (MAGE_INTELLIGENCE_PRM / 100.0));

        double actualDPS = this.mage.getHeroDPS();
        assertEquals(expectedValue, actualDPS, 0.0);
    }

    @Test
    public void getDPS_withOnlyWeapon_returnsCorrectValue() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.setWeapon(this.staff);

        double weaponDps = this.staff.getDPS();

        double expectedValue = weaponDps * (1 + (MAGE_INTELLIGENCE_PRM / 100.0));

        double actualDPS = this.mage.getHeroDPS();
        assertEquals(expectedValue, actualDPS, 0.0);
    }

    @Test
    public void getDPS_withOnlyWeaponWhenLevelUp_returnsCorrectValue() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.levelUp();
        this.mage.levelUp();
        this.mage.levelUp();
        this.mage.setWeapon(this.staff);

        double primeAttr = MAGE_INTELLIGENCE_PRM + MAGE_INTELLIGENCE_LVL_UP * 3;

        double weaponDps = this.staff.getDPS();

        double expectedValue = weaponDps * (1 + (primeAttr / 100.0));

        double actualDPS = this.mage.getHeroDPS();
        assertEquals(expectedValue, actualDPS, 0.0);
    }

    @Test
    public void getDPS_withOnlyWeaponAndArmorWhenLevelUp_returnsCorrectValue() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.levelUp();
        this.mage.levelUp();
        this.mage.levelUp();
        this.mage.setWeapon(this.staff);
        this.mage.setBodyArmor(cloth);

        double primeAttr = PRIME_CLOTH_ATTR_VAL + MAGE_INTELLIGENCE_PRM + MAGE_INTELLIGENCE_LVL_UP * 3;

        double weaponDps = this.staff.getDPS();

        double expectedValue = weaponDps * (1 + (primeAttr / 100.0));

        double actualDPS = this.mage.getHeroDPS();
        assertEquals(expectedValue, actualDPS, 0.0);
    }

    @Test
    public void equipItem_AddsBodyArmorToTheEquipmentCorrect() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.equipItem(this.cloth);
        Item actualArmor = mage.getBodyArmor();

        assertEquals(this.cloth, actualArmor);
    }

    @Test
    public void equipItem_AddsHeadArmorToTheEquipmentCorrect() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.equipItem(this.cap);
        Item actualArmor = mage.getHeadArmor();

        assertEquals(this.cap, actualArmor);
    }

    @Test
    public void equipItem_AddsHandsArmorToTheEquipmentCorrect() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.equipItem(this.clothGloves);

        Item actualArmor = mage.getHandsArmor();
        assertEquals(this.clothGloves, actualArmor);
    }

    @Test
    public void equipItem_AddsLegsArmorToTheEquipmentCorrect() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.equipItem(this.boots);

        Item actualArmor = mage.getLegsArmor();
        assertEquals(this.boots, actualArmor);
    }

    @Test
    public void equipItem_AddsWeaponToTheEquipmentCorrect() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        this.mage.equipItem(this.staff);

        Weapon actualWeapon = mage.getWeapon();
        assertEquals(this.staff, actualWeapon);
    }

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testAttackMethodPrintsAndLevelsUp() {
        this.mage.attack();

        String expected = String.format("You raised your level. Current level %d%n", mage.getLevel());
        assertEquals(expected, outContent.toString());

        int expectedLevel = 2;

        int actualLevel = mage.getLevel();
        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void showEquipmentReturnsCorrectString() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        String expected = "Slot -> HEAD Item -> Armor type: Cap, Protection: 11, Primary attribute value: 6" + System.lineSeparator() +
                "Slot -> HANDS Item -> Armor type: ClothGloves, Protection: 4, Primary attribute value: 5" + System.lineSeparator() +
                "Slot -> BODY Item -> Armor type: Cloth, Protection: 10, Primary attribute value: 11" + System.lineSeparator() +
                "Slot -> LEGS Item -> Armor type: Boots, Protection: 12, Primary attribute value: 9" + System.lineSeparator() +
                "Slot -> WEAPON Item -> Weapon type: Staff, Damage: 32, Attack speed: 1,10 DPS: 35,20" + System.lineSeparator();

        this.mage.setBodyArmor(this.cloth);
        this.mage.setHeadArmor(this.cap);
        this.mage.setHandsArmor(this.clothGloves);
        this.mage.setLegsArmor(this.boots);
        this.mage.setWeapon(this.staff);

        String actualEquipment = this.mage.showEquipment();

        assertEquals(expected.trim(), actualEquipment);
    }

    @Test
    public void showStatsReturnsCorrectString() throws IllegalWeaponException, IllegalArmorException, IllegalItemException {

        String expected = "Hero name: Petar\n" +
                "Hero level: 2\n" +
                "Stats:\n" +
                "STRENGTH: 2\n" +
                "DEXTERITY: 2\n" +
                "INTELLIGENCE: 44\n" +
                "VITALITY: 110" + System.lineSeparator() +
                "Hero DPS: 50,69";

        this.mage.setBodyArmor(this.cloth);
        this.mage.setHeadArmor(this.cap);
        this.mage.setHandsArmor(this.clothGloves);
        this.mage.setLegsArmor(this.boots);
        this.mage.setWeapon(this.staff);

        this.mage.levelUp();

        String actualStats = this.mage.getStats();
        assertEquals(expected.trim(), actualStats);
    }

    @Test
    public void SetProtection_ToArmorSetsCorrectValue() {
        this.cloth.setProtection(100);

        int actualProtection = cloth.getProtection();
        assertEquals(100, actualProtection);
    }

    @Test
    public void setDamage_SetsTheDamageCorrectToTheWeapon() {
        this.axe.setDamage(100);

        int actualDamage = this.axe.getDamage();
        assertEquals(100, actualDamage);
    }

    @Test
    public void setDurability_SetsTheDurabilityCorrectToTheItem() {
        this.sword.setDurability(178);

        int actualDurability = this.sword.getDurability();
        assertEquals(178, actualDurability);
    }

    @Test
    public void setForLevel_SetsTheRequiredLevelCorrectToTheItem() {
        this.staff.setForLevel(12);

        int actualForLevel = this.staff.getForLevel();
        assertEquals(12, actualForLevel);
    }
}
