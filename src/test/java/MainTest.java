import core.Engine;
import exceptions.EngineInstanceException;
import org.junit.Before;
import org.junit.Test;

public class MainTest {
    private Engine engine;

    @Before
    public void setUp() throws EngineInstanceException {
       this.engine = new Engine();
    }

    @Test(expected = EngineInstanceException.class)
    public void secondInstanceOfEngineThrows() throws EngineInstanceException {
        engine = new Engine();
    }
}
