import common.AttrNames;
import models.attributes.Attributes;
import models.attributes.BaseAttributes;
import models.attributes.TotalAttributes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class AttributesTest {
    private Attributes baseAttrs;
    private Attributes totalAttrs;

    private static final int STRENGTH = 10;
    private static final int DEXTERITY = 11;
    private static final int INTELLIGENCE = 12;
    private static final int VITALITY = 14;

    private static final int SET_STRENGTH = 15;
    private static final int SET_DEXTERITY = 18;
    private static final int SET_INTELLIGENCE = 3;
    private static final int SET_VITALITY = 4;

    private static final Map<AttrNames, Integer> mock_allAttrs = new HashMap<>();

    @Before
    public void setUp() {
        this.baseAttrs = new BaseAttributes(STRENGTH, DEXTERITY, INTELLIGENCE, VITALITY);
        this.totalAttrs = new TotalAttributes(STRENGTH, DEXTERITY, INTELLIGENCE, VITALITY);

        mock_allAttrs.put(AttrNames.STRENGTH, STRENGTH);
        mock_allAttrs.put(AttrNames.DEXTERITY, DEXTERITY);
        mock_allAttrs.put(AttrNames.INTELLIGENCE, INTELLIGENCE);
        mock_allAttrs.put(AttrNames.VITALITY, VITALITY);
    }

    @Test
    public void testBaseAttr_initialisation_shouldCreateValidObject() {
        int actualStrength = baseAttrs.getStrength();
        Assert.assertEquals(STRENGTH, actualStrength);

        int actualDexterity = baseAttrs.getDexterity();
        Assert.assertEquals(DEXTERITY, actualDexterity);

        int actualIntelligence = baseAttrs.getIntelligence();
        Assert.assertEquals(INTELLIGENCE, actualIntelligence);

        int actualVitality = baseAttrs.getVitality();
        Assert.assertEquals(VITALITY, actualVitality);
    }

    @Test
    public void testTotalAttr_initialisation_shouldCreateValidObject() {
        int actualStrength = totalAttrs.getStrength();
        Assert.assertEquals(STRENGTH, actualStrength);

        int actualDexterity = totalAttrs.getDexterity();
        Assert.assertEquals(DEXTERITY, actualDexterity);

        int actualIntelligence = totalAttrs.getIntelligence();
        Assert.assertEquals(INTELLIGENCE, actualIntelligence);

        int actualVitality = totalAttrs.getVitality();
        Assert.assertEquals(VITALITY, actualVitality);
    }

    @Test
    public void baseAttrs_setStrength_shouldSetCorrectValue() {
        this.baseAttrs.setStrength(SET_STRENGTH);

        int actualStrength = this.baseAttrs.getStrength();
        Assert.assertEquals(SET_STRENGTH, actualStrength);
    }

    @Test
    public void baseAttrs_setDexterity_shouldSetCorrectValue() {
        this.baseAttrs.setDexterity(SET_DEXTERITY);

        int actualDexterity = this.baseAttrs.getDexterity();
        Assert.assertEquals(SET_DEXTERITY, actualDexterity);
    }

    @Test
    public void baseAttrs_setIntelligence_shouldSetCorrectValue() {
        this.baseAttrs.setIntelligence(SET_INTELLIGENCE);

        int actualIntelligence = this.baseAttrs.getIntelligence();
        Assert.assertEquals(SET_INTELLIGENCE, actualIntelligence);
    }

    @Test
    public void baseAttrs_setVitality_shouldSetCorrectValue() {
        this.baseAttrs.setVitality(SET_VITALITY);

        int actualVitality = this.baseAttrs.getVitality();
        Assert.assertEquals(SET_VITALITY, actualVitality);
    }

    @Test
    public void totalAttrs_setStrength_shouldSetCorrectValue() {
        this.totalAttrs.setStrength(SET_STRENGTH);

        int actualStrength = this.totalAttrs.getStrength();
        Assert.assertEquals(SET_STRENGTH, actualStrength);
    }

    @Test
    public void totalAttrs_setDexterity_shouldSetCorrectValue() {
        this.totalAttrs.setDexterity(SET_DEXTERITY);

        int actualDexterity = this.totalAttrs.getDexterity();
        Assert.assertEquals(SET_DEXTERITY, actualDexterity);
    }

    @Test
    public void totalAttrs_setIntelligence_shouldSetCorrectValue() {
        this.totalAttrs.setIntelligence(SET_INTELLIGENCE);

        int actualIntelligence = this.totalAttrs.getIntelligence();
        Assert.assertEquals(SET_INTELLIGENCE, actualIntelligence);
    }

    @Test
    public void totalAttrs_setVitality_shouldSetCorrectValue() {
        this.totalAttrs.setVitality(SET_VITALITY);

        int actualVitality = this.totalAttrs.getVitality();
        Assert.assertEquals(SET_VITALITY, actualVitality);
    }

    @Test
    public void baseAttrs_getAllAttrs_returnsCorrect() {
        Map<AttrNames, Integer> allBaseAttrs = this.baseAttrs.getAllAttrs();

        Integer expectedStrength = mock_allAttrs.get(AttrNames.STRENGTH);
        Integer actualStrength = allBaseAttrs.get(AttrNames.STRENGTH);

        Assert.assertEquals(expectedStrength, actualStrength);

        Integer expectedDexterity = mock_allAttrs.get(AttrNames.DEXTERITY);
        Integer actualDexterity = allBaseAttrs.get(AttrNames.DEXTERITY);

        Assert.assertEquals(expectedDexterity, actualDexterity);

        Integer expectedIntelligence = mock_allAttrs.get(AttrNames.INTELLIGENCE);
        Integer actualIntelligence = allBaseAttrs.get(AttrNames.INTELLIGENCE);

        Assert.assertEquals(expectedIntelligence, actualIntelligence);

        Integer expectedVitality = mock_allAttrs.get(AttrNames.VITALITY);
        Integer actualVitality = allBaseAttrs.get(AttrNames.VITALITY);

        Assert.assertEquals(expectedVitality, actualVitality);

    }

    @Test
    public void totalAttrs_getAllAttrs_returnsCorrect() {
        Map<AttrNames, Integer> allTotalAttrs = this.totalAttrs.getAllAttrs();

        Integer expectedStrength = mock_allAttrs.get(AttrNames.STRENGTH);
        Integer actualStrength = allTotalAttrs.get(AttrNames.STRENGTH);

        Assert.assertEquals(expectedStrength, actualStrength);

        Integer expectedDexterity = mock_allAttrs.get(AttrNames.DEXTERITY);
        Integer actualDexterity = allTotalAttrs.get(AttrNames.DEXTERITY);

        Assert.assertEquals(expectedDexterity, actualDexterity);

        Integer expectedIntelligence = mock_allAttrs.get(AttrNames.INTELLIGENCE);
        Integer actualIntelligence = allTotalAttrs.get(AttrNames.INTELLIGENCE);

        Assert.assertEquals(expectedIntelligence, actualIntelligence);

        Integer expectedVitality = mock_allAttrs.get(AttrNames.VITALITY);
        Integer actualVitality = allTotalAttrs.get(AttrNames.VITALITY);

        Assert.assertEquals(expectedVitality, actualVitality);
    }
}
