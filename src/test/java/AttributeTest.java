import models.attributes.Attribute;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AttributeTest {
    private static final int VAL = 10;
    private static final int VAL_SET = 25;

    private Attribute attr;

    @Before
    public void setUp() {
        this.attr = new Attribute(VAL);
    }

    @Test
    public void testGetVal_validParam_returnsCorrect() {
        int actual = this.attr.getValue();
        Assert.assertEquals(VAL, actual);
    }

    @Test
    public void testSetValue_validParam_addsCorrect() {
        this.attr.setValue(VAL_SET);
        int actual = this.attr.getValue();
        Assert.assertEquals(VAL_SET, actual);
    }
}