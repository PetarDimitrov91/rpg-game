package exceptions;

public class IllegalItemException extends CustomException{
    public IllegalItemException(String message) {
        super(message);
    }
}
