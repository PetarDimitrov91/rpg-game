package exceptions;

public class IllegalArmorException extends CustomException {
    public IllegalArmorException(String message) {
        super(message);
    }
}
