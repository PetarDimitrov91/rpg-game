package exceptions;

public class IllegalWeaponException extends CustomException {
    public IllegalWeaponException(String message) {
        super(message);
    }
}
