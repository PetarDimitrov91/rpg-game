package exceptions;

import static common.ExceptionMessages.ENGINE_EXCEPTION;

public class EngineInstanceException extends CustomException {
    public EngineInstanceException() {
        super(ENGINE_EXCEPTION);
    }
}
