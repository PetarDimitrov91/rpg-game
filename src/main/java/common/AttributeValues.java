package common;

public class AttributeValues {
    public static final int DEF_LEVEL = 1;

    public static final double DEF_CHR_DMG = 1.0;

    public static final int DEF_HERO_VITALITY = 100;
    public static final int DEF_HERO_VITALITY_LVL_UP = 10;

    public static final int DEF_TOTAL_ATTR = 0;

    public static final int MAGE_STRENGTH = 1;
    public static final int MAGE_DEXTERITY = 1;
    public static final int MAGE_INTELLIGENCE_PRM = 8;

    public static final int MAGE_STRENGTH_LVL_UP = 1;
    public static final int MAGE_DEXTERITY_LVL_UP = 1;
    public static final int MAGE_INTELLIGENCE_LVL_UP = 5;

    public static final int RANGER_STRENGTH = 1;
    public static final int RANGER_DEXTERITY_PRM = 7;
    public static final int RANGER_INTELLIGENCE = 1;

    public static final int RANGER_STRENGTH_LVL_UP = 1;
    public static final int RANGER_DEXTERITY_PRM_LVL_UP = 5;
    public static final int RANGER_INTELLIGENCE_LVL_UP = 1;

    public static final int ROGUE_STRENGTH = 2;
    public static final int ROGUE_DEXTERITY_PRM = 6;
    public static final int ROGUE_INTELLIGENCE = 1;

    public static final int ROGUE_STRENGTH_LVL_UP = 1;
    public static final int ROGUE_DEXTERITY_PRM_LVL_UP= 4;
    public static final int ROGUE_INTELLIGENCE_LVL_UP = 1;

    public static final int WARRIOR_STRENGTH_PRM = 5;
    public static final int WARRIOR_DEXTERITY = 2;
    public static final int WARRIOR_INTELLIGENCE = 1;

    public static final int WARRIOR_STRENGTH_PRM_LVL_UP = 3;
    public static final int WARRIOR_DEXTERITY_LVL_UP = 2;
    public static final int WARRIOR_INTELLIGENCE_LVL_UP = 1;

    public static final int PRIME_CLOTH_ATTR_VAL = 11;
    public static final int PRIME_LEATHER_ATTR_VAL = 13;
    public static final int PRIME_MAIL_ATTR_VAL = 14;
    public static final int PRIME_PLATE_ATTR_VAL = 17;

    public static final int PRIME_CLOTH_GLOVES_ATTR_VAL = 5;
    public static final int PRIME_LEATHER_GLOVES_ATTR_VAL = 7;
    public static final int PRIME_MAIL_GLOVES_ATTR_VAL = 9;

    public static final int PRIME_CAP_ATTR_VAL = 6;
    public static final int PRIME_HELM_ATTR_VAL = 11;

    public static final int PRIME_BOOTS_ATTR_VAL = 9;
    public static final int PRIME_MAIL_BOOTS_ATTR_VAL = 15;
}
