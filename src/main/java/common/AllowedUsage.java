package common;

import java.util.Set;

import static common.AllowedUsageConst.*;

public enum AllowedUsage {
    Mage(MAGE_ALLOWED_USAGES),
    Ranger(RANGER_ALLOWED_USAGES),
    Rogue(ROGUE_ALLOWED_USAGES),
    Warrior(WARRIOR_ALLOWED_USAGES);

    private Set<String> items;

    AllowedUsage(Set<String> items) {
        this.items = items;
    }

    public Set<String> getValue() {
        return this.items; }
}
