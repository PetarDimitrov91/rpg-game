package common;

public class ExceptionMessages {
    public static final String ENGINE_EXCEPTION = "An Engine can be instantiated only 1 time";
    public static final String ITEM_NOT_ALLOWED = "Hero class cannot carry this item. Please choose another item";
    public static final String ITEM_LEVEL_EX_MSG = "Your hero cannot equip this item because the item required level is greater that your hero level.";
}
