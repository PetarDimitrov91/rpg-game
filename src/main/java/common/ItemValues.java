package common;

public class ItemValues {
    //head
    public static final int CAP_PROTECTION = 11;
    public static final int CAP_LEVEL = 1;
    public static final int HELM_PROTECTION = 16;
    public static final int HELM_LEVEL = 2;

    //body
    public static final int CLOTH_PROTECTION = 10;
    public static final int CLOTH_LEVEL = 1;
    public static final int LATHER_PROTECTION = 16;
    public static final int LATHER_LEVEL = 1;
    public static final int MAIL_PROTECTION = 21;
    public static final int MAIL_LEVEL = 1;
    public static final int PLATE_PROTECTION = 32;
    public static final int PLATE_LEVEL = 1;

    // hands
    public static final int CLOTH_GLOVES_PROTECTION = 4;
    public static final int CLOTH_GLOVES_LEVEL = 1;
    public static final int LEATHER_GLOVES_PROTECTION = 9;
    public static final int LEATHER_GLOVES_LEVEL = 1;
    public static final int MAIL_GLOVES_PROTECTION = 14;
    public static final int MAIL_GLOVES_LEVEL = 3;

    // legs
    public static final int BOOTS_PROTECTION = 12;
    public static final int BOOTS_LEVEL = 1;
    public static final int MAIL_BOOTS_PROTECTION = 18;
    public static final int MAIL_BOOTS_LEVEL = 3;

    //weapons
    public static final int AXE_DAMAGE = 21;
    public static final int AXE_LEVEL = 2;
    public static final int BOW_DAMAGE = 31;
    public static final int BOW_LEVEL = 1;
    public static final int DAGGER_DAMAGE = 16;
    public static final int DAGGER_LEVEL = 1;
    public static final int HAMMER_DAMAGE = 45;
    public static final int HAMMER_LEVEL = 3;
    public static final int STAFF_DAMAGE = 32;
    public static final int STAFF_LEVEL = 1;
    public static final int SWORD_DAMAGE = 30;
    public static final int SWORD_LEVEL = 1;
    public static final int WAND_DAMAGE = 22;
    public static final int WAND_LEVEL = 1;

    //-----:
    public static final double AXE_ATTACK_SPEED = 0.5;
    public static final double BOW_ATTACK_SPEED = 1.5;
    public static final double DAGGER_ATTACK_SPEED = 2;
    public static final double HAMMER_ATTACK_SPEED = 0.2;
    public static final double STAFF_ATTACK_SPEED = 1.1;
    public static final double SWORD_ATTACK_SPEED = 1.2;
    public static final double WAND_ATTACK_SPEED = 1.2;

    public static final int DEF_DURABILITY = 100;
}
