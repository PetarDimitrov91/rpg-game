package common;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AllowedUsageConst {
    public static final Set<String> MAGE_ALLOWED_USAGES = new HashSet<>(List.of("Staff", "Wand", "Cloth", "Cap", "ClothGloves", "Boots"));
    public static final Set<String> RANGER_ALLOWED_USAGES = new HashSet<>(List.of("Bow", "Leather", "Mail", "Cap", "LeatherGloves"));
    public static final Set<String> ROGUE_ALLOWED_USAGES = new HashSet<>(List.of("Dagger", "Sword", "Leather", "Mail", "Cap", "Helm", "ClothGloves", "LeatherGloves", "Boots"));
    public static final Set<String> WARRIOR_ALLOWED_USAGES = new HashSet<>(List.of("Axe", "Hammer", "Sword", "Mail", "Plate", "Cap", "Helm", "LeatherGloves", "MailGloves", "Boots", "MailBoots"));
}
