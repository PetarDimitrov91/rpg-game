package common;

public enum AttrNames {
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE,
    VITALITY
}
