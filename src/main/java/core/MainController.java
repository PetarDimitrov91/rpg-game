package core;

import exceptions.CustomException;
import models.characters.Mage;
import models.characters.Ranger;
import models.characters.Rogue;
import models.characters.Warrior;
import models.characters.interfaces.Hero;
import models.items.Item;
import models.items.armor.body.Cloth;
import models.items.armor.body.Leather;
import models.items.armor.body.Mail;
import models.items.armor.body.Plate;
import models.items.armor.hands.ClothGloves;
import models.items.armor.hands.LeatherGloves;
import models.items.armor.hands.MailGloves;
import models.items.armor.head.Cap;
import models.items.armor.head.Helm;
import models.items.armor.legs.Boots;
import models.items.armor.legs.MailBoots;
import models.items.weapons.*;

import java.util.Scanner;

import static common.AllowedUsageConst.*;

public class MainController {
    private static final Scanner console = new Scanner(System.in);

    protected static void startGame() {
        System.out.printf("Hello user!%nIf you want to play, you can choose from 4 types of Characters. Just type the hero class.%n");
        System.out.println();
        System.out.printf("The Characters are:%nMage%nRanger%nRogue%nWarrior%n");
        System.out.print("Please type your Character class:");

        String pickedHero = console.nextLine().trim().toLowerCase();

        System.out.printf("You must name your character.%nPlease enter a name:");

        String heroName = console.nextLine();

        Hero hero = createHero(pickedHero, heroName);

        while (hero == null) {
            System.out.println("Please chose another character class");
            pickedHero = console.nextLine().trim().toLowerCase();
            hero = createHero(pickedHero, heroName);
        }

        System.out.println("Now, if you want, you can equip your hero. Please type \"yes\" if you want and \"no\" if don't want");

        String command = console.nextLine().trim().toLowerCase();

        switch (command) {
            case "yes" -> equipHero(hero);
            case "no" -> {
                System.out.println("Your character is has not any equipment are you sure? Just type \"yes\" or \"no\" for confirmation.");
                String confirmation = console.nextLine().trim().toLowerCase();

                if (confirmation.equals("no")) {
                    equipHero(hero);
                }
            }
        }

        System.out.println("Now your character is ready to begin the game. Do you want to do it? Just type \"yes\" or \"no\" for confirmation.");
        String confirmation = console.nextLine().trim().toLowerCase();

        if (confirmation.equals("yes")) {
            beginGame(hero);
        } else {
            System.out.println("Good bye!");
        }
    }

    private static Hero createHero(String pickedHero, String name) {
        Hero hero;
        switch (pickedHero) {
            case "mage" -> hero = new Mage(name);
            case "rogue" -> hero = new Rogue(name);
            case "ranger" -> hero = new Ranger(name);
            case "warrior" -> hero = new Warrior(name);
            default -> {
                System.out.println("There is no such character class");
                return null;
            }
        }

        return hero;
    }

    private static void equipHero(Hero pickedHero) {
        System.out.println("We have a different items in the game, but beware, not every character class can equip every item");
        System.out.println("Here you can see which ca character class which item can cary:");

        System.out.println();

        System.out.printf("Mage -> %s%n", String.join(", ", MAGE_ALLOWED_USAGES));
        System.out.printf("Ranger -> %s%n", String.join(", ", RANGER_ALLOWED_USAGES));
        System.out.printf("Rogue -> %s%n", String.join(", ", ROGUE_ALLOWED_USAGES));
        System.out.printf("Warrior -> %s%n", String.join(", ", WARRIOR_ALLOWED_USAGES));

        System.out.println();

        System.out.println("Now please type the selected item. Beware of typing the items exactly as you see them above. The command is case insensitive");
        System.out.println("Example:");
        System.out.println("LeatherGloves -> valid");
        System.out.println("Leather Gloves -> not valid");
        System.out.println("leathergloves -> valid");
        System.out.println("leather gloves -> not valid");
        System.out.println("When you are done, just type \"done\"");

        String chosenItem = console.nextLine().trim().toLowerCase();

        while (!chosenItem.equals("done")) {

            Item item = createItem(chosenItem);

            if (item == null) {
                chosenItem = console.nextLine().trim().toLowerCase();
                continue;
            }

            try {
                pickedHero.equipItem(item);
            } catch (CustomException e) {
                System.out.println(e.getMessage());
            }

            chosenItem = console.nextLine();
        }
    }

    private static Item createItem(String chosenItem) {
        Item item;

        switch (chosenItem) {
            case "axe" -> item = new Axe();
            case "hammer" -> item = new Hammer();
            case "sword" -> item = new Sword();
            case "dagger" -> item = new Dagger();
            case "bow" -> item = new Bow();
            case "staff" -> item = new Staff();
            case "wand" -> item = new Wand();
            case "cloth" -> item = new Cloth();
            case "leather" -> item = new Leather();
            case "mail" -> item = new Mail();
            case "plate" -> item = new Plate();
            case "cap" -> item = new Cap();
            case "helm" -> item = new Helm();
            case "clothgloves" -> item = new ClothGloves();
            case "leathergloves" -> item = new LeatherGloves();
            case "mailgloves" -> item = new MailGloves();
            case "boots" -> item = new Boots();
            case "mailboots" -> item = new MailBoots();
            default -> {
                System.out.println("There is no such item. Chose another one.");
                return null;
            }
        }

        return item;
    }

    private static int currRow = 0;
    private static int currCol = 0;
    private static boolean hasWon = false;

    private static String[][] labyrinth = {
            {"p", "*", "m", "_", "m", "_"},
            {"_", "_", "_", "_", "*", "_"},
            {"_", "*", "m", "*", "*", "m"},
            {"m", "*", "*", "*", "_", "_"},
            {"_", "*", "_", "m", "m", "*"},
            {"m", "_", "_", "*", "_", "e"},
    };

    private static void beginGame(Hero hero) {
        System.out.println("Now we are in the game!");
        System.out.println("Hire, the following commands are available:");

        System.out.println("command: end  -> closes the application");
        System.out.println("command: stats  -> show your character stats");
        System.out.println("command: equipment -> show character's equipment");
        System.out.println("command: explore  -> the main part of the game starts. You must go through a labyrinth");

        String command = console.nextLine().trim().toLowerCase();

        while (true) {
            switch (command) {
                case "end" -> {
                    return;
                }

                case "stats" -> System.out.println(hero.getStats());
                case "equipment" -> System.out.println(hero.showEquipment());
                case "explore" -> explore(hero);
            }

            if (hasWon) {
                break;
            }

            command = console.nextLine().trim().toLowerCase();
        }
    }

    private static void explore(Hero hero) {
        System.out.println("Hire we go, naw you must go through a labyrinth");
        System.out.println("Only 4 commands are allowed: \"up\", \"down\", \"left\", \"right\" or \"end\" if you want to end the game.");


        printLabyrinth();

        System.out.println("this is your labyrinth.");
        System.out.println("\"p\" stays for current player position");
        System.out.println("\"m\" stays for monster");
        System.out.println("\"*\" stays for wall");
        System.out.println("\"_\" stays for free path");
        System.out.println("\"e\" stays exit from labyrinth");

        System.out.println("Your task is go through the labyrinth and reach the exit using only the commands described above");

        System.out.println("Please enter your move");
        String command = console.nextLine().trim().toLowerCase();

        while (!command.equals("end") && !hasWon) {

            switch (command) {
                case "up" -> movePlayer(hero, currRow - 1, currCol);
                case "down" -> movePlayer(hero, currRow + 1, currCol);
                case "left" -> movePlayer(hero, currRow, currCol - 1);
                case "right" -> movePlayer(hero, currRow, currCol + 1);
                default -> System.out.println("Not valid move.");
            }

            if (!hasWon) {
                System.out.println("Please enter your move");
                command = console.nextLine().trim().toLowerCase();
            } else {
                break;
            }
        }

        System.out.println(hero.getStats());

        if (command.equals("end")) {
            System.out.println("You are now out from the main part of the game. If you want to end the application just type end!");
            System.out.println("Or you can type other command like \"stats\", \"explore\" or \"equipment\"");

            labyrinth[currRow][currCol] = "_";
            currRow = 0;
            currCol = 0;
            labyrinth[currRow][currCol] = "p";

        }
    }

    private static void movePlayer(Hero hero, int row, int col) {
        if (!isInBounds(row, col)) {
            System.out.println("You are going out of the labyrinth. Try another move");
            return;
        }

        switch (labyrinth[row][col]) {
            case "*" -> {
                System.out.println("There is a wall. Try another move");
                return;
            }
            case "m" -> {
                System.out.println("you came across a monster, you will fight!");
                hero.attack();
            }
            case "e" -> {
                System.out.println("Huraaa you found the exit!");
                hasWon = true;
            }
        }

        labyrinth[currRow][currCol] = "_";
        labyrinth[row][col] = "p";

        currRow = row;
        currCol = col;
        printLabyrinth();
    }

    private static boolean isInBounds(int row, int col) {
        return row >= 0 && row < labyrinth.length && col >= 0 && col < labyrinth.length;
    }

    private static void printLabyrinth() {
        for (String[] row : labyrinth) {
            System.out.println(String.join("  ", row));
        }
        System.out.println();
    }
}
