package core;

import exceptions.EngineInstanceException;

public class Engine implements Runnable {
    private static byte instance;

    public Engine() throws EngineInstanceException {
        if (instance == 1) {
            throw new EngineInstanceException();
        } else {
            instance = 1;
        }
    }

    @Override
    public void run() {
        MainController.startGame();
    }
}
