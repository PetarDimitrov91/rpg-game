package models.attributes;

public class TotalAttributes extends Attributes {
    public TotalAttributes(int strength, int dexterity, int intelligence, int vitality) {
        super(strength, dexterity, intelligence, vitality);
    }
}
