package models.attributes;

public class BaseAttributes extends Attributes{
    public BaseAttributes(int strength, int dexterity, int intelligence, int vitality) {
        super(strength, dexterity, intelligence, vitality);
    }
}
