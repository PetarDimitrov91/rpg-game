package models.attributes;

import common.AttrNames;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class Attributes {
    private final Attribute strength;
    private final Attribute dexterity;
    private final Attribute intelligence;
    private final Attribute vitality;

    public Attributes(int strength, int dexterity, int intelligence, int vitality) {
        this.strength = new Attribute(strength);
        this.dexterity = new Attribute(dexterity);
        this.intelligence = new Attribute(intelligence);
        this.vitality = new Attribute(vitality);
    }

    public int getStrength() {
        return strength.getValue();
    }

    public void setStrength(int strength) {
        this.strength.setValue(strength);
    }

    public int getDexterity() {
        return dexterity.getValue();
    }

    public void setDexterity(int dexterity) {
        this.dexterity.setValue(dexterity);
    }

    public int getIntelligence() {
        return intelligence.getValue();
    }

    public void setIntelligence(int intelligence) {
        this.intelligence.setValue(intelligence);
    }

    public int getVitality() {
        return vitality.getValue();
    }

    public void setVitality(int vitality) {
        this.vitality.setValue(vitality);
    }

    public Map<AttrNames, Integer> getAllAttrs() {
        Map<AttrNames, Integer> allAttrs = new LinkedHashMap<>();
        allAttrs.put(AttrNames.STRENGTH, this.strength.getValue());
        allAttrs.put(AttrNames.DEXTERITY, this.dexterity.getValue());
        allAttrs.put(AttrNames.INTELLIGENCE, this.intelligence.getValue());
        allAttrs.put(AttrNames.VITALITY, this.vitality.getValue());

        return allAttrs;
    }
}
