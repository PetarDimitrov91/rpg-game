package models.characters.interfaces;

import exceptions.IllegalArmorException;
import exceptions.IllegalItemException;
import exceptions.IllegalWeaponException;
import common.Slots;
import models.attributes.Attribute;
import models.attributes.Attributes;
import models.items.Item;
import models.items.weapons.Weapon;
import repositories.Inventory;

import java.util.Map;

public interface Hero {

    String getName();

    int getLevel();

    Attribute getPrimAttr();

    Attribute getTotalPrimAttr();

    Attributes getBaseAttrs();

    Attributes getTotalAttrs();

    Map<Slots, Item> getEquipment();

    void setHeadArmor(Item headArmor) throws IllegalWeaponException, IllegalArmorException, IllegalItemException;

    void setBodyArmor(Item bodyArmor) throws IllegalWeaponException, IllegalArmorException, IllegalItemException;

    void setLegsArmor(Item legsArmor) throws IllegalWeaponException, IllegalArmorException, IllegalItemException;

    void setHandsArmor(Item handsArmor) throws IllegalArmorException, IllegalItemException, IllegalWeaponException;

    void setWeapon(Item weapon) throws IllegalWeaponException, IllegalArmorException, IllegalItemException;

    void equipItem(Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException;

    Item getHeadArmor();

    Item getBodyArmor();

    Item getLegsArmor();

    Item getHandsArmor();

    Weapon getWeapon();

    Inventory<Item> getInventory();

    double getHeroDPS();

    void setHeroDPS(double heroDPS);

    void calculateDamage();

    void attack();

    void levelUp();

    void storeItem(Item item);

    String getStats();

    String showEquipment();
}
