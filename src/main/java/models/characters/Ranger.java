package models.characters;

import models.attributes.BaseAttributes;
import static common.AttributeValues.*;

public class Ranger extends BaseHero {
    public Ranger(String name) {
        super(name, new BaseAttributes(RANGER_STRENGTH, RANGER_DEXTERITY_PRM,RANGER_INTELLIGENCE,DEF_HERO_VITALITY),RANGER_DEXTERITY_PRM);
    }


    @Override
    public void levelUp() {
        super.increaseAttrs(RANGER_STRENGTH_LVL_UP, RANGER_DEXTERITY_PRM_LVL_UP, RANGER_INTELLIGENCE_LVL_UP, RANGER_DEXTERITY_PRM_LVL_UP);
    }
}
