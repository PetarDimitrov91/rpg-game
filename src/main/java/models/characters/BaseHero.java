package models.characters;

import common.AllowedUsage;
import common.Slots;
import exceptions.IllegalArmorException;
import exceptions.IllegalItemException;
import exceptions.IllegalWeaponException;
import models.attributes.Attribute;
import models.attributes.Attributes;
import models.attributes.TotalAttributes;
import models.characters.interfaces.Hero;
import models.items.Item;
import models.items.armor.Armor;
import models.items.armor.body.BodyArmor;
import models.items.armor.hands.HandsArmor;
import models.items.armor.head.HeadArmor;
import models.items.armor.legs.LegsArmor;
import models.items.weapons.Weapon;
import repositories.Equipment;
import repositories.Inventory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static common.AttributeValues.*;
import static common.ExceptionMessages.ITEM_LEVEL_EX_MSG;
import static common.ExceptionMessages.ITEM_NOT_ALLOWED;

abstract class BaseHero implements Hero {
    private final String name;
    private int level;
    private final Attributes baseAttributes;
    private final Attributes totalAttributes;
    private final Attribute primAttr;
    private final Attribute totalPrimAttr;
    private final Equipment<Item> equipment;
    private final Inventory<Item> inventory;
    private double heroDPS;

    public BaseHero(String name, Attributes baseAttributes, int primeAttrVal) {
        this.name = name;
        this.level = DEF_LEVEL;
        this.baseAttributes = baseAttributes;
        this.totalAttributes = new TotalAttributes(baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence(), baseAttributes.getVitality());
        this.primAttr = new Attribute(primeAttrVal);
        this.totalPrimAttr = new Attribute(primeAttrVal);
        this.equipment = new Equipment<>();
        this.inventory = new Inventory<>();
        calculateDamage();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public Attributes getBaseAttrs() {
        return this.baseAttributes;
    }

    @Override
    public Attribute getPrimAttr() {
        return primAttr;
    }

    @Override
    public Attribute getTotalPrimAttr() {
        return totalPrimAttr;
    }

    @Override
    public Attributes getTotalAttrs() {
        return this.totalAttributes;
    }

    @Override
    public Item getHeadArmor() {
        return this.equipment.getHeroEquipment().get(Slots.HEAD);
    }

    @Override
    public Item getBodyArmor() {
        return this.equipment.getHeroEquipment().get(Slots.BODY);
    }

    @Override
    public Item getLegsArmor() {
        return this.equipment.getHeroEquipment().get(Slots.LEGS);
    }

    @Override
    public Item getHandsArmor() {
        return this.equipment.getHeroEquipment().get(Slots.HANDS);
    }

    @Override
    public Weapon getWeapon() {
        return (Weapon) this.equipment.getHeroEquipment().get(Slots.WEAPON);
    }

    public Map<Slots, Item> getEquipment() {
        return this.equipment.getHeroEquipment();
    }

    @Override
    public Inventory<Item> getInventory() {
        return this.inventory;
    }

    @Override
    public double getHeroDPS() {
        return heroDPS;
    }

    @Override
    public void setHeroDPS(double heroDPS) {
        this.heroDPS = heroDPS;
    }

    @Override
    public void storeItem(Item item) {
        this.inventory.addItem(item);
    }

    private void setTotalPrimAttrVal(Armor item) {
        int value = item.getPrimaryAttribute().getValue();
        int crrPrimAttrVal = this.totalPrimAttr.getValue();
        this.totalPrimAttr.setValue(value + crrPrimAttrVal);
    }

    @Override
    public void setHeadArmor(Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        setItem(Slots.HEAD, item);
    }

    @Override
    public void setBodyArmor(Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        setItem(Slots.BODY, item);
    }

    @Override
    public void setLegsArmor(Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        setItem(Slots.LEGS, item);
    }

    @Override
    public void setHandsArmor(Item item) throws IllegalArmorException, IllegalItemException, IllegalWeaponException {
        setItem(Slots.HANDS, item);
    }

    @Override
    public void setWeapon(Item weapon) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        setItem(Slots.WEAPON, weapon);
        calculateDamage();
    }

    @Override
    public void equipItem(Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        if (item instanceof BodyArmor) {
            setBodyArmor(item);
        } else if (item instanceof HandsArmor) {
            setHandsArmor(item);
        } else if (item instanceof HeadArmor) {
            setHeadArmor(item);
        } else if (item instanceof LegsArmor) {
            setLegsArmor(item);
        } else if (item instanceof Weapon) {
            setWeapon(item);
        }
    }

    private void setItem(Slots slot, Item item) throws IllegalWeaponException, IllegalArmorException, IllegalItemException {
        String heroClassName = this.getClass().getSimpleName();

        boolean isValidItem = validate(heroClassName, slot, item);

        if (!isValidItem) {
            if (slot == Slots.WEAPON) {
                throw new IllegalWeaponException(String.format("%s %s", heroClassName, ITEM_NOT_ALLOWED));
            } else {
                throw new IllegalArmorException(String.format("%s %s", heroClassName, ITEM_NOT_ALLOWED));
            }
        }

        if (item.getForLevel() > this.getLevel()) {
            throw new IllegalItemException(ITEM_LEVEL_EX_MSG);
        }

        if (getEquipment().get(slot) != null) {
            storeItem(getEquipment().get(slot));
        }

        getEquipment().put(slot, item);

        if (slot != Slots.WEAPON) {
            setValuesUp((Armor) item);
        }

        setTotalAttrs();
    }

    private boolean validate(String heroClassName, Slots slot, Item item) {
        Set<String> allowedItems;

        switch (heroClassName.toLowerCase()) {
            case "mage" -> {
                allowedItems = AllowedUsage.Mage.getValue();
                return validateHero(allowedItems, slot, item);
            }
            case "ranger" -> {
                allowedItems = AllowedUsage.Ranger.getValue();
                return validateHero(allowedItems, slot, item);
            }
            case "rogue" -> {
                allowedItems = AllowedUsage.Rogue.getValue();
                return validateHero(allowedItems, slot, item);
            }
            case "warrior" -> {
                allowedItems = AllowedUsage.Warrior.getValue();
                return validateHero(allowedItems, slot, item);
            }
        }

        return false;
    }

    private boolean validateHero(Set<String> allowedItems, Slots slot, Item item) {
        Slots itemSlot = item.getSlot();
        String name = item.getClass().getSimpleName();

        return allowedItems.contains(name) && slot == itemSlot;
    }

    private void setValuesUp(Armor item) {
        setTotalPrimAttrVal(item);
        calculateDamage();
    }

    @Override
    public void attack() {
        levelUp();
        System.out.printf("You raised your level. Current level %d%n", getLevel());
    }

    @Override
    public void calculateDamage() {
        int primeAttrVal = this.totalPrimAttr.getValue();
        double weaponDps = getWeapon() != null ? getWeapon().getDPS() : DEF_CHR_DMG;

        // double additionalDamage = primeAttrVal * 0.01;

         /*
              the variable additionalDamage is commented because I was not sure about the assignment's specs.
              It is said that "
                        • Strength – determines the physical strength of the character.
                        o Each point of strength increases a Warriors damage by 1%.
                        • Dexterity – determines the character's ability to attack with speed and nimbleness.
                        o Each point of dexterity increases a Rangers and Rogues damage by 1%.
                        • Intelligence – determines the character's affinity with magic.
                        o Each point of intelligence increases a Mages damage by 1%."

              but on the last slide of the specs the calculations are made without adding this 1% for each of primary attribute.
              That's why this line is commented out.
              If I'm wrong, then the variable additionalDamage can be simply added to the setHeroDPS method like this:
              "this.setHeroDPS(weaponDps * ((1 + primeAttrVal / 100.0)+ additionalDamage));"
      */

        this.setHeroDPS(weaponDps * (1 + primeAttrVal / 100.0));
    }


    private void setTotalAttrs() {
        String className = this.getClass().getSimpleName();

        switch (className.toLowerCase()) {
            case "mage" -> this.totalAttributes.setIntelligence(this.totalPrimAttr.getValue());
            case "ranger", "rogue" -> this.totalAttributes.setDexterity(this.totalPrimAttr.getValue());
            case "warrior" -> this.totalAttributes.setStrength(this.totalPrimAttr.getValue());
        }
    }

    protected void increaseAttrs(int strength, int dexterity, int intelligence, int primeAttrVal) {
        int currentStrength = this.baseAttributes.getStrength();
        this.baseAttributes.setStrength(currentStrength + strength);

        int currentDexterity = this.baseAttributes.getDexterity();
        this.baseAttributes.setDexterity(currentDexterity + dexterity);

        int currentIntelligence = this.baseAttributes.getIntelligence();
        this.baseAttributes.setIntelligence(currentIntelligence + intelligence);

        int currentVitality = this.baseAttributes.getVitality();
        this.baseAttributes.setVitality(currentVitality + DEF_HERO_VITALITY_LVL_UP);

        int crrTtlStrength = this.totalAttributes.getStrength();
        this.totalAttributes.setStrength(crrTtlStrength + strength);

        int crrTtlDexterity = this.totalAttributes.getDexterity();
        this.totalAttributes.setDexterity(crrTtlDexterity + dexterity);

        int crrTtlIntelligence = this.totalAttributes.getIntelligence();
        this.totalAttributes.setIntelligence(crrTtlIntelligence + intelligence);

        int crrTtlVitality = this.totalAttributes.getVitality();
        this.totalAttributes.setVitality(crrTtlVitality + DEF_HERO_VITALITY_LVL_UP);

        int crrPrimeAttrVal = this.primAttr.getValue();
        this.primAttr.setValue(crrPrimeAttrVal + primeAttrVal);

        int crrTtlPrimAttrValue = this.totalPrimAttr.getValue();
        this.totalPrimAttr.setValue(crrTtlPrimAttrValue + primeAttrVal);

        this.level++;
        calculateDamage();
        setTotalAttrs();
    }

    @Override
    public String showEquipment() {
        StringBuilder sb = new StringBuilder();
        Map<Slots, Item> equipment = getEquipment();

        equipment.forEach((k, v) -> sb.append(String.format("Slot -> %s Item -> %s%n", k.toString(), v != null ? v.toString() : "empty")));

        return sb.toString().trim();
    }

    @Override
    public String getStats() {
        StringBuilder sb = new StringBuilder();

        String[] tmp = new String[4];

        AtomicInteger counter = new AtomicInteger(0);

        getTotalAttrs().getAllAttrs()
                .forEach((k, v) -> {
                    int i = counter.getAndAdd(1);
                    tmp[i] = String.format("%s: %d", k.toString(), v);
                });

        sb.append(String.format("Hero name: %s\n", getName()))
                .append(String.format("Hero level: %d\n", getLevel()))
                .append("Stats:\n")
                .append(String.join("\n", tmp)).append(System.lineSeparator())
                .append(String.format("Hero DPS: %.2f", getHeroDPS()));

        return sb.toString().trim();
    }
}
