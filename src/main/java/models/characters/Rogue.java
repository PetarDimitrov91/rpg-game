package models.characters;

import models.attributes.BaseAttributes;
import static common.AttributeValues.*;

public class Rogue extends BaseHero {
    public Rogue(String name) {
        super(name,new BaseAttributes(ROGUE_STRENGTH, ROGUE_DEXTERITY_PRM,ROGUE_INTELLIGENCE,DEF_HERO_VITALITY),ROGUE_DEXTERITY_PRM);
    }



    @Override
    public void levelUp() {
        super.increaseAttrs(ROGUE_STRENGTH_LVL_UP, ROGUE_DEXTERITY_PRM_LVL_UP, ROGUE_INTELLIGENCE_LVL_UP, ROGUE_DEXTERITY_PRM_LVL_UP);
    }
}
