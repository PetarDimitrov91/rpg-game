package models.characters;

import models.attributes.BaseAttributes;
import static common.AttributeValues.*;

public class Warrior extends BaseHero {
    public Warrior(String name) {
        super(name, new BaseAttributes(WARRIOR_STRENGTH_PRM, WARRIOR_DEXTERITY, WARRIOR_INTELLIGENCE, DEF_HERO_VITALITY), WARRIOR_STRENGTH_PRM);
    }



    @Override
    public void levelUp() {
        super.increaseAttrs(WARRIOR_STRENGTH_PRM_LVL_UP, WARRIOR_DEXTERITY_LVL_UP, WARRIOR_INTELLIGENCE_LVL_UP, WARRIOR_STRENGTH_PRM_LVL_UP);
    }
}
