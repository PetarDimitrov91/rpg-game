package models.characters;

import models.attributes.BaseAttributes;
import static common.AttributeValues.*;

public class Mage extends BaseHero {
    public Mage(String name) {
        super(name, new BaseAttributes(MAGE_STRENGTH, MAGE_DEXTERITY, MAGE_INTELLIGENCE_PRM, DEF_HERO_VITALITY), MAGE_INTELLIGENCE_PRM);
    }


    @Override
    public void levelUp() {
        super.increaseAttrs(MAGE_STRENGTH_LVL_UP, MAGE_DEXTERITY_LVL_UP, MAGE_INTELLIGENCE_LVL_UP, MAGE_INTELLIGENCE_LVL_UP);
    }
}
