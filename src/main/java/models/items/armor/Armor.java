package models.items.armor;


import common.Slots;
import models.attributes.Attribute;
import models.items.Item;

public abstract class Armor extends Item {
    private int protection;
    private final Attribute primaryAttribute;

    public Armor(int protection, Slots slot, Attribute attr,int forLevel) {
        super(slot,forLevel);
        this.protection = protection;
        this.primaryAttribute = attr;
    }

    public int getProtection() {
        return protection;
    }

    public void setProtection(int protection) {
        this.protection = protection;
    }

    public Attribute getPrimaryAttribute() {
        return primaryAttribute;
    }

    @Override
    public String toString() {
        return String.format("Armor type: %s, Protection: %d, Primary attribute value: %d",
                this.getClass().getSimpleName(),
                getProtection(),
                getPrimaryAttribute().getValue()
        );
    }
}
