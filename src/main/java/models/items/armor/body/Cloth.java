package models.items.armor.body;

import static common.AttributeValues.PRIME_CLOTH_ATTR_VAL;
import static common.ItemValues.CLOTH_LEVEL;
import static common.ItemValues.CLOTH_PROTECTION;

public class Cloth extends BodyArmor {
    public Cloth() {
        super(CLOTH_PROTECTION,PRIME_CLOTH_ATTR_VAL,CLOTH_LEVEL);
    }
}
