package models.items.armor.body;

import static common.AttributeValues.PRIME_MAIL_ATTR_VAL;
import static common.ItemValues.MAIL_LEVEL;
import static common.ItemValues.MAIL_PROTECTION;

public class Mail extends BodyArmor {
    public Mail() {
        super(MAIL_PROTECTION,PRIME_MAIL_ATTR_VAL,MAIL_LEVEL);
    }
}
