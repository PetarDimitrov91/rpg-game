package models.items.armor.body;

import static common.AttributeValues.PRIME_LEATHER_ATTR_VAL;
import static common.ItemValues.LATHER_LEVEL;
import static common.ItemValues.LATHER_PROTECTION;

public class Leather extends BodyArmor {
    public Leather() {
        super(LATHER_PROTECTION,PRIME_LEATHER_ATTR_VAL,LATHER_LEVEL);
    }
}
