package models.items.armor.body;

import static common.AttributeValues.PRIME_PLATE_ATTR_VAL;
import static common.ItemValues.PLATE_LEVEL;
import static common.ItemValues.PLATE_PROTECTION;

public class Plate extends BodyArmor {
    public Plate() {
        super(PLATE_PROTECTION, PRIME_PLATE_ATTR_VAL, PLATE_LEVEL);
    }
}
