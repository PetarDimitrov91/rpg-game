package models.items.armor.body;

import common.Slots;
import models.attributes.Attribute;
import models.items.armor.Armor;

public abstract class BodyArmor extends Armor {
    public BodyArmor(int protection, int primeAttrVal, int forLevel) {
        super(protection, Slots.BODY, new Attribute(primeAttrVal),forLevel);
    }
}
