package models.items.armor.head;

import common.Slots;
import models.attributes.Attribute;
import models.items.armor.Armor;

public abstract class HeadArmor extends Armor {
    public HeadArmor(int protection, int primeAttrVal,int forLevel) {
        super(protection, Slots.HEAD,new Attribute(primeAttrVal),forLevel);
    }
}
