package models.items.armor.head;

import static common.AttributeValues.PRIME_HELM_ATTR_VAL;
import static common.ItemValues.HELM_LEVEL;
import static common.ItemValues.HELM_PROTECTION;

public class Helm extends HeadArmor {
    public Helm() {
        super(HELM_PROTECTION,PRIME_HELM_ATTR_VAL,HELM_LEVEL);
    }
}
