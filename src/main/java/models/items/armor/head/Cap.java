package models.items.armor.head;

import static common.AttributeValues.PRIME_CAP_ATTR_VAL;
import static common.ItemValues.CAP_LEVEL;
import static common.ItemValues.CAP_PROTECTION;

public class Cap extends HeadArmor {
    public Cap() {
        super(CAP_PROTECTION,PRIME_CAP_ATTR_VAL,CAP_LEVEL);
    }
}
