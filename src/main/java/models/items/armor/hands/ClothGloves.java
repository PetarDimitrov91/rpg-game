package models.items.armor.hands;

import static common.AttributeValues.PRIME_CLOTH_GLOVES_ATTR_VAL;
import static common.ItemValues.CLOTH_GLOVES_LEVEL;
import static common.ItemValues.CLOTH_GLOVES_PROTECTION;

public class ClothGloves extends HandsArmor {
    public ClothGloves() {
        super(CLOTH_GLOVES_PROTECTION,PRIME_CLOTH_GLOVES_ATTR_VAL,CLOTH_GLOVES_LEVEL);
    }
}
