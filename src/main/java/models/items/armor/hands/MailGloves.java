package models.items.armor.hands;

import static common.AttributeValues.PRIME_MAIL_GLOVES_ATTR_VAL;
import static common.ItemValues.MAIL_GLOVES_LEVEL;
import static common.ItemValues.MAIL_GLOVES_PROTECTION;

public class MailGloves extends HandsArmor {
    public MailGloves() {
        super(MAIL_GLOVES_PROTECTION,PRIME_MAIL_GLOVES_ATTR_VAL,MAIL_GLOVES_LEVEL);
    }
}
