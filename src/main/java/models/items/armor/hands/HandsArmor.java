package models.items.armor.hands;

import common.Slots;
import models.attributes.Attribute;
import models.items.armor.Armor;

public abstract class HandsArmor extends Armor {
    public HandsArmor(int protection, int primeAttrVal, int forLevel) {
        super(protection, Slots.HANDS,new Attribute(primeAttrVal),forLevel);
    }
}
