package models.items.armor.hands;

import static common.AttributeValues.PRIME_LEATHER_GLOVES_ATTR_VAL;
import static common.ItemValues.LEATHER_GLOVES_LEVEL;
import static common.ItemValues.LEATHER_GLOVES_PROTECTION;

public class LeatherGloves extends HandsArmor {
    public LeatherGloves() {
        super(LEATHER_GLOVES_PROTECTION,PRIME_LEATHER_GLOVES_ATTR_VAL,LEATHER_GLOVES_LEVEL);
    }
}
