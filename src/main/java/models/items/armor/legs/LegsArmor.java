package models.items.armor.legs;

import common.Slots;
import models.attributes.Attribute;
import models.items.armor.Armor;

public abstract class LegsArmor extends Armor {
    public LegsArmor(int protection, int primeAttrVal,int forLevel) {
        super(protection, Slots.LEGS,new Attribute(primeAttrVal),forLevel);
    }
}
