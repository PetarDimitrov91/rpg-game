package models.items.armor.legs;

import static common.AttributeValues.PRIME_MAIL_BOOTS_ATTR_VAL;
import static common.ItemValues.MAIL_BOOTS_LEVEL;
import static common.ItemValues.MAIL_BOOTS_PROTECTION;

public class MailBoots extends LegsArmor {
    public MailBoots() {
        super(MAIL_BOOTS_PROTECTION, PRIME_MAIL_BOOTS_ATTR_VAL,MAIL_BOOTS_LEVEL);
    }
}
