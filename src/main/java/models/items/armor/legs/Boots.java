package models.items.armor.legs;

import static common.AttributeValues.PRIME_BOOTS_ATTR_VAL;
import static common.ItemValues.BOOTS_LEVEL;
import static common.ItemValues.BOOTS_PROTECTION;

public class Boots extends LegsArmor {
    public Boots() {
        super(BOOTS_PROTECTION, PRIME_BOOTS_ATTR_VAL, BOOTS_LEVEL);
    }
}
