package models.items;

import common.Slots;

import static common.ItemValues.DEF_DURABILITY;

public abstract class Item {
    private int durability;
    private int forLevel;
    private final Slots slot;

    public Item(Slots slot, int forLevel) {
        this.slot = slot;
        this.forLevel = forLevel;
        this.durability = DEF_DURABILITY;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public Slots getSlot() {
        return this.slot;
    }

    public abstract String toString();

    public int getForLevel() {
        return forLevel;
    }

    public void setForLevel(int forLevel) {
        this.forLevel = forLevel;
    }
}
