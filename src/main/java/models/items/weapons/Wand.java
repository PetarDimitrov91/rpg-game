package models.items.weapons;

import static common.ItemValues.*;

public class Wand extends Weapon {
    public Wand() {
        super(WAND_DAMAGE, WAND_ATTACK_SPEED,WAND_LEVEL);
    }
}
