package models.items.weapons;

import common.Slots;
import models.items.Item;

public abstract class Weapon extends Item {
    private int damage;
    private final double attackSpeed;

    public Weapon(int damage, double attackSpeed,int forLevel) {
        super(Slots.WEAPON, forLevel);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }


    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getDPS() {
        return this.damage * attackSpeed;
    }

    @Override
    public String toString() {
        return String.format("Weapon type: %s, Damage: %d, Attack speed: %.2f DPS: %.2f",
                this.getClass().getSimpleName(),
                getDamage(),
                getAttackSpeed(),
                getDPS()
        );
    }
}
