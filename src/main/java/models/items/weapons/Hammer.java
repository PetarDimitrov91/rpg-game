package models.items.weapons;

import static common.ItemValues.*;

public class Hammer extends Weapon {
    public Hammer() {
        super(HAMMER_DAMAGE, HAMMER_ATTACK_SPEED,HAMMER_LEVEL);
    }
}
