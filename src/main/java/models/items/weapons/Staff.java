package models.items.weapons;

import static common.ItemValues.*;

public class Staff extends Weapon {
    public Staff() {
        super(STAFF_DAMAGE, STAFF_ATTACK_SPEED,STAFF_LEVEL);
    }
}
