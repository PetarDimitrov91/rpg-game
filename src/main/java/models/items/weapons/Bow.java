package models.items.weapons;

import static common.ItemValues.*;

public class Bow extends Weapon {
    public Bow() {
        super(BOW_DAMAGE, BOW_ATTACK_SPEED,BOW_LEVEL);
    }
}
