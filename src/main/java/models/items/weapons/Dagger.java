package models.items.weapons;

import static common.ItemValues.*;

public class Dagger extends Weapon {
    public Dagger() {
        super(DAGGER_DAMAGE, DAGGER_ATTACK_SPEED,DAGGER_LEVEL);
    }
}
