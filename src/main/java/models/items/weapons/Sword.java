package models.items.weapons;

import static common.ItemValues.*;

public class Sword extends Weapon{
    public Sword() {
        super(SWORD_DAMAGE, SWORD_ATTACK_SPEED,SWORD_LEVEL);
    }
}
