package models.items.weapons;

import static common.ItemValues.*;

public class Axe extends Weapon {
    public Axe() {
        super(AXE_DAMAGE, AXE_ATTACK_SPEED,AXE_LEVEL);
    }
}
