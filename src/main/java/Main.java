import core.Engine;

public class Main {
    public static void main(String[] args) throws Exception {
        Engine e = new Engine();
        e.run();
    }
}
