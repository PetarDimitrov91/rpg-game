package repositories;

import java.util.ArrayList;
import java.util.List;

public class Inventory<T> {
    private List<T> inventory;

    public Inventory(){
        this.inventory = new ArrayList<>();
    }

    public List<T> getItems(){
        return this.inventory;
    }

    public void addItem(T item){
        this.inventory.add(item);
    }
}
