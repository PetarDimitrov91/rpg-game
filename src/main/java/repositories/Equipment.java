package repositories;

import common.Slots;

import java.util.LinkedHashMap;
import java.util.Map;

public class Equipment<T> {
    private final Map<Slots, T> heroEquipment;

    public Equipment() {
        this.heroEquipment = new LinkedHashMap<>();
        this.heroEquipment.put(Slots.HEAD, null);
        this.heroEquipment.put(Slots.HANDS, null);
        this.heroEquipment.put(Slots.BODY, null);
        this.heroEquipment.put(Slots.LEGS, null);
        this.heroEquipment.put(Slots.WEAPON, null);
    }

    public Map<Slots, T> getHeroEquipment() {
        return heroEquipment;
    }
}
